## METEO ![Meteo](data/meteo.png  "Meteo")

### Know the forecast of the next hours & days.

Developed with Vala & Gtk,using OpenWeatherMap API (https://openweathermap.org/)

![Screenshot](./data/screens/screenshot_1.png  "Meteo")

### Features:

- Current weather, with information about temperature, pressure, wind speed and direction, sunrise & sunset.
- Forecast for next 18 hours.
- Forecast for next five days.
- Choose your units (metric, imperial or british).
- Choose your city, with maps help.
- Awesome maps with weather info.
- System tray indicator.

----

### How To Install

#### For Ubuntu and derivates:

You can add my **PPA** at *Launchpad.net*, only for *bionic* and *disco* releases:

    sudo add-apt-repository ppa:bitseater/ppa
    sudo apt update
    sudo apt install com.gitlab.bitseater.meteo


#### For Debian and derivates:

You can download the last .deb package from:

[Package: Debian 10](https://gitlab.com/bitseater/meteo/-/jobs/artifacts/0.9.8/download?job=package%3Adebian)


#### For Fedora:
	
Meteo is availiable in [Official Fedora repo](https://apps.fedoraproject.org/packages/meteo):

    sudo dnf install meteo

Also, you can add my **Copr** repository:

	sudo dnf copr enable bitseater/meteo
	sudo dnf update
	sudo dnf install meteo


#### For Arch and derivates:

Available in the Arch User Repository: [meteo-gtk](https://aur.archlinux.org/packages/meteo-gtk/)

You will need an [AUR Helper](https://wiki.archlinux.org/index.php/AUR_helpers) like yay to install:

    yay -S meteo-gtk


#### Flatpak package:

You can install the flatpak package with:

    flatpak install --from https://flathub.org/repo/appstream/com.gitlab.bitseater.meteo.flatpakref

Or if you have added Flathub repository with: 

	flatpak install flathub com.gitlab.bitseater.meteo


#### Snap package:

Snap package is available at [Meteo in Snap Store](https://snapcraft.io/meteo)

I've added it at the request of the users, but I don't give support to Snap.You can install it, at your own risk, from terminal:

    sudo snap install meteo

#### Packaging status:

[![Packaging status](https://repology.org/badge/vertical-allrepos/meteo.svg)](https://repology.org/project/meteo/versions)

----

#### Indicator:

Many OS have discontinued the AppIndicator/Ayatana Indicator support. So in order to use it, you can install manually.

- For *Gnome* users, try [Appindicator Support extension](https://extensions.gnome.org/extension/615/appindicator-support/)  

- For *elementary OS* users, try [Entornos GNU Linux](http://entornosgnulinux.com/2018/08/15/como-instalar-wingpanel-indicator-ayatana-en-elementary-os-juno/)

- For *Linux Mint* users, you must to activate appindicator support:

 `Configuration -> Preferences -> General -> Activate indicator support (Cinnamon restart required)`

----
### How To Build

Library Dependencies :

- gtk+-3.0
- libsoup-2.4
- json-glib-1.0
- geocode-glib-1.0
- webkit2gtk-4.0
- appindicator3-0.1
- meson


For advanced users!

    git clone https://gitlab.com/bitseater/meteo.git
    cd meteo
    ./quick.sh -b

----

#### New on release 0.9.8:

- Indicator icon issue fixed.
- Fixed some lintian warnings on packaging.

Fixed issues: #125, #128, #129.

----
### Other screenshots:

**A map with temperatures by Dark Sky**
![Screenshot](./data/screens/screenshot_2.png  "Meteo")

**About Meteo window:**
![Screenshot](./data/screens/screenshot_3.png  "Meteo")

**Indicator in panel / system tray:**
![Screenshot](./data/screens/screenshot_4.png  "Meteo")
